package br.edu.unisep.customers.controller;

import br.edu.unisep.customers.domain.dto.CustomerDto;
import br.edu.unisep.customers.domain.usecase.FindAllCustomersUseCase;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/customer")
public class CustomerController {

    private FindAllCustomersUseCase findAllCustomersUseCase;

    @GetMapping
    public ResponseEntity<List<CustomerDto>> findAll() {
        var customers = findAllCustomersUseCase.execute();
        return ResponseEntity.ok(customers);
    }

}
