package br.edu.unisep.customers.domain.usecase;

import br.edu.unisep.customers.data.repository.CustomerRepository;
import br.edu.unisep.customers.domain.builder.CustomerBuilder;
import br.edu.unisep.customers.domain.dto.CustomerDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class FindAllCustomersUseCase {

    private final CustomerRepository customerRepository;
    private final CustomerBuilder customerBuilder;

    public List<CustomerDto> execute() {
        var customers = customerRepository.findAll();

        return customerBuilder.from(customers);
    }
}
