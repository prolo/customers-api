package br.edu.unisep.customers.domain.builder;

import br.edu.unisep.customers.data.entity.Customer;
import br.edu.unisep.customers.domain.dto.CustomerDto;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@Component
public class CustomerBuilder {

    public List<CustomerDto> from(List<Customer> customers) { return customers.stream().map(this::from).collect(Collectors.toList()); }

    public CustomerDto from(Customer customer) {
        return new CustomerDto(
                customer.getId(),
                customer.getName(),
                customer.getEmail(),
                customer.getCpf(),
                customer.getEmail(),
                customer.getAddresses()
        );
    }
}
