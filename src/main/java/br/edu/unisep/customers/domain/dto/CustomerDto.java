package br.edu.unisep.customers.domain.dto;

import br.edu.unisep.customers.data.entity.Address;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@Getter
@AllArgsConstructor
public class CustomerDto {

    private Integer id;
    private String name;
    private String email;
    private String cpf;
    private String birthdate;
    private List<Address> addresses;

}
