package br.edu.unisep.customers.data.repository;

import br.edu.unisep.customers.data.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository extends JpaRepository<Customer, Integer> {
}
